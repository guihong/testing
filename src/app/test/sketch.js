let circles;
let seed = Math.floor(Math.random() * 1000000);
let hash = CryptoJS.SHA256(seed.toString()).toString();
console.log(hash);

function setup() {
	pixelDensity(1);
	randomSeed(seed);
	initializeFields();
	createCanvas(600, 600);
	background(0);
}

function draw() {
	let total = 50;
	let count = 0;
	let attempts = 0;
	let number_of_attempts = 1000000;
	let number_of_circles = 5000;

	while (circles.length == 0) {
		let bigCircle = newCircle(240, 240);
		if (bigCircle != null) {
			circles.push(new Array(bigCircle));
			bigCircle.show();
		}
	}

	while (count < total) {
		let maxDiameter = 100;
		let minDiameter = 1;
		let newC = newCircle(minDiameter, maxDiameter);
		if (newC !== null) {
			circles.push(new Array(newC));
			newC.show();
			count++;
		}
		attempts++;
		if (attempts > number_of_attempts) {
			fill(360, 360, 360, 2);
			noStroke();
			rect(0, 0, 600, 600);
			noLoop();
			console.log("Finished");
			save(`${seed}.png`);
			break;
		} else if (circles.length == number_of_circles) {
			fill(360, 360, 360, 2);
			noStroke();
			rect(0, 0, 600, 600);
			noLoop();
			console.log("Finished");
			save(`${seed}.png`);
			break;
		}
	}
}

function newCircle(minDiameter, maxDiameter) {
	let r = random(minDiameter, maxDiameter);
	let x = random(r, width - r);
	let y = random(r, height - r);
	let valid = true;

	for (let c = 0; c < circles.length; c++) {
		let d = dist(circles[c][0].x, circles[c][0].y, x, y);
		if (d - 1 < circles[c][0].r + r) {
			valid = false;
			break;
		}
	}
	if (valid) {
		return new Circle(r, x, y);
	} else {
		return null;
	}
}

class Circle {
	constructor(r_, x_, y_) {
		this.r = r_;
		this.x = x_;
		this.y = y_;
	}

	show() {
		let r = this.r;
		let x = this.x;
		let y = this.y;
		let chance = random(1, 6);
		colorMode(HSB, 360, 100, 100, 100);
		let brightColorWarm = color(random(1, 360), 10, 100);
		let darkColorWarm = color(random(1, 360), random(90, 100), random(5, 25));
		let brightColorCool = color(random(1, 360), random(9, 20), 100);
		let darkColorCool = color(random(1, 360), random(90, 100), random(10, 40));
		let brightColorMix = color(random(1, 360), random(5, 15), random(90, 100));
		// the size of the circles to be red
		if (r >= 20) {
			// change to any color
			colorMode(HSB, 360, 100, 100, 100);
			brightColorWarm = color(random(1, 15), 10, 100);
			darkColorWarm = color(random(1, 15), random(90, 100), random(5, 25));
			brightColorCool = color(random(1, 15), random(9, 20), 100);
			darkColorCool = color(random(1, 15), random(90, 100), random(10, 40));
			brightColorMix = color(random(1, 15), random(5, 15), random(90, 100));
		}
		// the outer circle to be what color
		if (r == 240) {
			// change the fill to any color
			colorMode(HSB, 360, 100, 100, 100);
			let red1 = color(random(1, 15), random(90, 100), random(5, 25));
			let red2 = color(random(1, 15), 10, 100);
			fill(red1);
			noStroke();
			ellipse(x, y, r * 2, r * 2);
			fill(red2);
			noStroke();
			ellipse(x, y, r * 2 - 20, r * 2 - 20);
		} else if (r < 600) {
			fill(brightColorCool);
			noStroke();
			ellipse(x, y, r * 2, r * 2);
		}
		let a6 = int(random((r * 2 * 5) / 7, r * 2));
		let a5 = int(random((a6 * 50) / 100, a6 - 1));
		let a4 = int(random((a5 * 50) / 100, a5 - 1));
		let a3 = int(random((a4 * 50) / 100, a4 - 1));
		let a2 = int(random((a3 * 50) / 100, a3 - 1));
		let a1 = int(random((a2 * 50) / 100, a2 - 1));
		for (let a = 6; a >= 0; a -= 1) {
			if (a == 6) {
				fill(darkColorWarm);
				noStroke();
				ellipse(x, y, a6, a6);
			} else if (a == 5) {
				fill(brightColorWarm);
				noStroke();
				ellipse(x, y, a5, a5);
			} else if (a == 4) {
				fill(darkColorCool);
				noStroke();
				ellipse(x, y, a4, a4);
			} else if (a == 3) {
				fill(brightColorCool);
				noStroke();
				ellipse(x, y, a3, a3);
			} else if (a == 2) {
				fill(darkColorWarm);
				noStroke();
				ellipse(x, y, a2, a2);
			} else if (a == 1) {
				fill(brightColorWarm);
				noStroke();
				ellipse(x, y, a1, a1);
			} else if (a == 0) {
				fill(brightColorMix);
				noStroke();
				ellipse(x, y, (r * 2 * 1) / 7, (r * 2 * 1) / 7);
			}
		}
		if (r >= 30) {
			if (chance <= 4) {
				colorMode(HSB, 360, 100, 100, 100);
				fill(0, 100, 100);
				noStroke();
				ellipse(x, y, 2, 2);
			}
		}
		// the inner most layers of the largest circle
		if (r == 240) {
			// 2nd inner most layer color
			colorMode(RGB, 255, 255, 255);
			fill(255, 0, 0);
			noStroke();
			ellipse(x, y, (r * 2 * 0.5) / 7, (r * 2 * 0.5) / 7);
			// inner most layer color
			colorMode(RGB, 255, 255, 255);
			fill(255, 255, 255);
			noStroke();
			ellipse(x, y, 10, 10);
		}
	}
}

function initializeFields() {
	circles = [];
	r = 0;
	x = 0;
	y = 0;
}
