import { Component, OnInit } from '@angular/core';
import * as p5 from 'p5';

@Component({
  selector: 'app-bulat',
  templateUrl: './bulat.component.html',
  styleUrls: ['./bulat.component.css']
})
export class BulatComponent implements OnInit {


  private p5;
  constructor() { }

  ngOnInit() {
    
    this.createCanvas();
}

private createCanvas() {
  this.p5 = new p5(this.sketch);
}

private sketch(p: any) {
  p.setup = () => {
    p.createCanvas(700, 600);
  };

  p.draw = () => {
    p.background(255);
    p.rect(100, 100, 100, 100);
  };
  }
  

}
