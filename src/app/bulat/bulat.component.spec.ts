import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulatComponent } from './bulat.component';

describe('BulatComponent', () => {
  let component: BulatComponent;
  let fixture: ComponentFixture<BulatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
